﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Source_Application.Service;
using System.Threading.Tasks;

namespace Source_Application.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SourceController : ControllerBase
    {
        private readonly ILogger<SourceController> _logger;
        private readonly TargetClient _targetClient;

        public SourceController(ILogger<SourceController> logger, TargetClient targetClient)
        {
            this._logger = logger;
            this._targetClient = targetClient;
        }
        // GET
        [HttpGet]
        [Route("testlogging")]
        public async Task<IActionResult> Get()
        {
            this._logger.LogInformation("Source application invoked");

            var headers = await _targetClient.SampleAsync();

            return Ok(headers);
        }
    }
}
