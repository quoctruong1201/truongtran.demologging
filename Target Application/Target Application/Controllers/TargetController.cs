﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Target_Application.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TargetController : ControllerBase
    {
        private readonly ILogger<TargetController> _logger;

        public TargetController(ILogger<TargetController> logger)
        {
            this._logger = logger;

        }

        // GET 
        [HttpGet]
        [Route("testlogging")]
        public ActionResult Get()
        {
            this._logger.LogInformation("Target method invoked");

            return Ok(Request.Headers);
        }
    }
}
