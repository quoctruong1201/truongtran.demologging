﻿using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Serilog;
using Serilog.Events;
using Serilog.Formatting.Json;
using System;

namespace Target_Application
{
    public class Program
    {
        public static void Main(string[] args)
        {
            // Add serilog for project
            Log.Logger = new LoggerConfiguration()
               .Enrich.FromLogContext()
               .MinimumLevel.Information()
               .WriteTo.Console(outputTemplate: "TimeStamp:[{Timestamp:yyyy-MM-dd HH:mm:ss.fff zzz}], LogLevel:[{Level:u3}], CorrelationId:[{CorrelationId}], Message:{Message:lj}{NewLine}{Exception}")
               .CreateLogger();
            try
            {
                CreateWebHostBuilder(args).Build().Run();
            }
            catch (Exception e)
            {
                Log.Error(e.Message);
                throw;
            }
            finally
            {
                // close log 
                Log.CloseAndFlush();
            }
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
              .UseSerilog()
                .UseStartup<Startup>();
    }
}
