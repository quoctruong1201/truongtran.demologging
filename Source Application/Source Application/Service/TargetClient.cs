﻿using System.Net.Http;
using System.Threading.Tasks;

namespace Source_Application.Service
{
    public class TargetClient
    {
        private readonly HttpClient _httpClient;

        public TargetClient(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }

        public async Task<string> SampleAsync()
        {
            var response = await _httpClient.GetAsync("api/Target/testlogging");

            response.EnsureSuccessStatusCode();

            var result = await response.Content.ReadAsStringAsync();
            return result;
        }
    }
}
